'use strict';

/**
 * libraries
 * @type {[type]}
 */
const express = require('express'),
			jwt = require('express-jwt'),
			rsaValidation = require('auth0-api-jwt-rsa-validation'),
			routes = require('./routes/routes.js'),
			app = express();

//init config server express
app.set('port', process.env.PORT || 3000);

//config access
app.use(function(req, res, next) {
	res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
  next();
});

//config routes
app.use('/', routes);

//middleware OAuth0  and json web token
console.log(rsaValidation());
const jwtCheck = jwt({
  secret: rsaValidation(),
  algorithms: ['RS256'],
  issuer: "https://juanmancb.auth0.com/",
  audience: 'https://apirest.com'
});

app.use(jwtCheck);

const guard = function(req, res, next) {
  console.log(req.user);
  switch(req.path) {
    case '/movies':
      var permissions = ['general'];
      for (var i = 0; i < permissions.length; i++) {
        if (req.users.scope.includes(permissions[i])) {
          next();
        } else {
          res.send(403, {message: 'Forbidden'});
        }
      }
      break;
    case '/publications':
      var permissions = ['general'];
      for (var i = 0; i < permissions.length; i++) {
        if (req.users.scope.includes(permissions[i])) {
          next();
        } else {
          res.send(403, {message: 'Forbidden'});
        }
      }
      break;
    case '/reviewers':
      var permissions = ['general'];
      for (var i = 0; i < permissions.length; i++) {
        if (req.users.scope.includes(permissions[i])) {
          next();
        } else {
          res.send(403, {message: 'Forbidden'});
        }
      }
      break;
    case '/pending':
      var permissions = ['admin'];
      console.log(req.user.scope);
      for (var i = 0; i < permissions.length; i++) {
        if (req.users.scope.includes(permissions[i])) {
          next();
        } else {
          res.send(403, {message: 'Forbidden'});
        }
      }
      break;
  }
};

app.use(guard);


app.use(function (err, req, res, next) {
  if (err.name === 'UnauthorizedError') {
    res.status(401).json({message:'Missing or invalid token'});
  }
});

/**
 * create server with express
 * @param  {[type]} ){} [description]
 * @return {[type]}       [description]
 */
app.listen(app.get('port'), function(){
	console.log("Express server listening on port " + app.get('port'));
});
