'use strict';

const express = require('express'),
			request = require('superagent'),
	 		router = express.Router();

//OAuth0 credential client 
var NON_INTERACTIVE_CLIENT_ID = 'BCBcKKCG0dKozYUg3aah10KVYfhlD800',
		NON_INTERACTIVE_CLIENT_SECRET = 'CPW-QB3TKjEGRtzDZnZvtj5SfdseD9ZBcDPmOsiltmPq2Q43lvMHmfJLdnTdPlSU';

var authData = {
	client_id: NON_INTERACTIVE_CLIENT_ID,
	client_secret: NON_INTERACTIVE_CLIENT_SECRET,
	grant_type: 'client_credentials',
	audience: 'https://apirest.com'
};

function getAccessToken(req, res, next){
  request
    .post('https://juanmancb.auth0.com/oauth/token')
    .send(authData)
    .end(function(err, res) {
      req.access_token = res.body.access_token
      next();
    })
}

router.get('/', function(req, res) {
	res.render('index');
});

router.get('/movies', getAccessToken, function(req, res) {
	request
		.get('http://localhost:3000/movies')
	  .set('Authorization', 'Bearer' + req.access_token)
  	.end(function(err, data) {
	 		if (data.status == 403) {
	 			res.send(403, 'Forbidden');
	 		} else {
	 			let movies = data.body;
	 			res.render('movies', {movies: movies});
	 		}
	 	})
});

router.get('/authors', getAccessToken, function(req, res) {
	request
		.get('http://localhost:3000/reviewers')
		.set('Authorization', 'Bearer' + req.access_token )
		.end(function(err, data) {
			if (data.status == 403) {
				res.send(403, 'Forbidden');
			} else {
				let authors = data.body;
				res.render('authors', {authors: authors});
			}
		})
});

router.get('/publications', getAccessToken, function(req, res) {
	request
		.get('http://localhost:3000/publications')
		.set('Authorization', 'Bearer' + req.access_token )
		.end(function(err, data) {
			if (data.status == 403) {
				res.send(403, 'Forbidden');
			} else {
				let publications = data.body;
				res.render('publications', {publications: publications});
			}
		})
});

router.get('/pending', getAccessToken, function(req, res) {
	request
		.get('http://localhost:3000/pending')
		.set('Authorization', 'Bearer' + req.access_token )
		.end(function(err, data) {
			if (data.status == 403) {
				res.send(403, 'Forbidden');
			} else {
				var movies = data.body;
        res.render('pending', {movies : movies});
			}
		})
});

module.exports = router;