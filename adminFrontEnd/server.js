'use strict';

/**
 * libraries
 * @type {[type]}
 */
const express = require('express'),
			path = require('path'), 
			favicon = require('serve-favicon'), 
			logger = require('morgan'),  
			cookieParser = require('cookie-parser'), 
			bodyParser = require('body-parser'),
      request = require('superagent'),
			//routes = require('./routes/routes.js'),
			app = express();

//init config server express
app.set('port', process.env.PORT || 3002);


//express configuration
// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');

app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

//config outes
//app.use('/', routes);

var NON_INTERACTIVE_CLIENT_ID = 'o9LMk1j8f6BfTXNAu5TZEHC7glB20JK5',
    NON_INTERACTIVE_CLIENT_SECRET = 'qd6B5vbv0twUYxbOk5MvK6TwC8pp4zUH1EywhwLEmqhM4Fz63v_o16a5AnD_hGTH';

var authData = {
  client_id: NON_INTERACTIVE_CLIENT_ID,
  client_secret: NON_INTERACTIVE_CLIENT_SECRET,
  grant_type: 'client_credentials',
  audience: 'https://apirest.com'
};

function getAccessToken(req, res, next){
  request
    .post('https://juanmancb.auth0.com/oauth/token')
    .send(authData)
    .end(function(err, res) {
      /*
      if (res.body.access_token) {
        req.access_token = res.body.access_token
        next();
      } else {
        res.send(401, 'Unauthoridez');
      }*/
      req.access_token = res.body.access_token
      next();
    })
}

app.use(getAccessToken);

app.get('/', function(req, res) {
  res.render('index');
});

app.get('/movies', getAccessToken, function(req, res) {
  request
    .get('http://localhost:3000/movies')
    .set('Authorization', 'Bearer' + req.access_token)
    .end(function(err, data) {
      if (data.status == 403) {
        res.send(403, 'Forbidden');
      } else {
        let movies = data.body;
        res.render('movies', {movies: movies});
      }
    })
});

app.get('/authors', getAccessToken, function(req, res) {
  request
    .get('http://localhost:3000/reviewers')
    .set('Authorization', 'Bearer' + req.access_token )
    .end(function(err, data) {
      if (data.status == 403) {
        res.send(403, 'Forbidden');
      } else {
        let authors = data.body;
        res.render('authors', {authors: authors});
      }
    })
});

app.get('/publications', getAccessToken, function(req, res) {
  request
    .get('http://localhost:3000/publications')
    .set('Authorization', 'Bearer' + req.access_token )
    .end(function(err, data) {
      if (data.status == 403) {
        res.send(403, 'Forbidden');
      } else {
        let publications = data.body;
        res.render('publications', {publications: publications});
      }
    })
});

app.get('/pending', getAccessToken, function(req, res) {
  request
    .get('http://localhost:3000/pending')
    .set('Authorization', 'Bearer' + req.access_token )
    .end(function(err, data) {
      if (data.status == 403) {
        res.send(403, 'Forbidden');
      } else {
        var movies = data.body;
        res.render('pending', {movies : movies});
      }
    })
});

// error handlers

// development error handler
// will print stacktrace
if (app.get('env') === 'development') {
  app.use(function(err, req, res, next) {
    res.status(err.status || 500);
    res.render('error', {
      message: err.message,
      error: err
    });
  });
}

// production error handler
// no stacktraces leaked to user
app.use(function(err, req, res, next) {
  res.status(err.status || 500);
  res.render('error', {
    message: err.message,
    error: {}
  });
});


// catch 404 and forward to error handler
app.use(function(req, res, next) {
  var err = new Error('Page Not Found, We Sorry :(');
  err.status = 404;
  next(err);
});


/**
 * create server with express
 * @param  {[type]} ){} [description]
 * @return {[type]}       [description]
 */
app.listen(app.get('port'), function(){
	console.log("Express server listening on port " + app.get('port'));
});
